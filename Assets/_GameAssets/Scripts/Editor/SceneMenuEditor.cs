﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEditor;

[ExecuteInEditMode]
public class SceneMenuEditor : Editor
{

    [MenuItem("Scenes/Drawing Board %#d", false, 0)]
    public static void OpenDrawingBoard()
    {
        OpenScene("00_DrawingBoard");
    }

    [MenuItem("Scenes/Main Menu", false, 1)]
    public static void OpenMainMenu()
    {
        OpenScene("00_DrawingBoard");
    }

    [MenuItem("Scenes/Credits", false, 9)]
    public static void OpenCredits()
    {
        OpenScene("99_Credits");
    }

    // Worlds Menus
    [MenuItem("Scenes/Worlds/Port (Tutorial)", false, 100)]
    public static void OpenWorldPortTut()
    {
        OpenScene("01_World_Port_Tutorial");
    }

    [MenuItem("Scenes/Worlds/Port", false, 101)]
    public static void OpenWorldPort()
    {
        OpenScene("01_World_Port");
    }




    [MenuItem("Scenes/Minigames/Trash Sorting/Port", false, 201)]
    public static void OpenMGTrashSortPort()
    {
        OpenScene("Minigames/MG_TrashSorting_Port");
    }

    [MenuItem("Scenes/Minigames/Delivery Driver/Port", false, 251)]
    public static void OpenMGDeliveryDriverPort()
    {
        OpenScene("Minigames/MG_DeliveryDriver_Port");
    }



    [MenuItem("Scenes/GameSparks Test Harness", false, 999)]
    public static void OpenGSTestHarness()
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.OpenScene("Assets/GameSparks/TestUI/GameSparksTestUI.unity");            
        }
    }


    public static void OpenScene(string sceneName)
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.OpenScene("Assets/_GameAssets/_Scenes/" + sceneName + ".unity");
        }
    }
 
}